# The GitLab documentation and the GitLab context-sensitive help

The official docs for the GitLab version in use are available at [serverurl]/help (e.g. **https://gitlab.gwdg.de/help**).

The use of many concepts and functions is facilitated by context-sensitive help: in many places in the UI, the terms used are provided with descriptions, tooltips or links to the relevant documentation section.
Example: 
* A link to the docs on SSH keys ("Learn more") is available in the description under *User Settings > SSH Keys > Add an SSH key*.
![screenshot](img/ssh_keys.jpeg)