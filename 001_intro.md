# Versioning and collaboration with GitLab. Relationship to Git and GitHub

## Version Control System (VCS).

**Git** is a free file version control system (VCS) software.  

Versioning makes the history of changes to a file or project visible (similar to the change tracking feature in Google Docs/Microsoft Office, etc.) and provides options such as: 
* go back to a previous version of the data
* make snapshots of project versions and make them available as releases
* work on parallel versions (branches) and apply all the changes as a block on completion (merge)

## Distributed Version Control System

One consequence of distributed version control is that work can be done locally (on one's own computer), **without a constant connection to the Git server**, because **Git repositories are "cloned"** as a whole. Changes are pushed to the remote server(s), and versions can be merged.
The smaller the **dataset** in the repo, the faster a repo is cloned. Code as well as text-based files of all kinds are well suited for versioning with Git, binary data (such as images or compiled code) less so, especially when large amounts of data are involved.

Git can **also be used only locally**, **when no collaboration is needed** (see also https://stackoverflow.com/questions/32406720/how-can-i-use-git-locally). **Git servers** (such as *github.com* or *gitlab.gwdg.de*) are the preffered means for **Git-based collaboration**. It is still possible to send changes by email and have someone merge them into the official code base, but Github and GitLab provide features that make this easier. 

## GitLab and GitHub

**Both GitLab and GitHub are based on Git (https://git-scm.com)** and have numerous features that make using Git as well as collaborating easier, such as:
* Hosting of repos
* A web-based interface that allows many Git actions directly in the browser, as well as creating and editing files.
* Features not available in Git (for user management, collaboration, project management, devops, etc.).

> Git is an open-source distributed version control system. **GitLab is built on top of Git.** [...] **You can do many Git operations directly in GitLab. However, the command line is required for advanced tasks, like fixing complex merge conflicts or rolling back commits.**

*From the [GitLab docs](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)*

**github.com** is operated by **Microsoft**, **gitlab.com** by **GitLab Inc.**. GitLab and GitHub (GitHub Enterprise only) can also be run on premises (this is the case with *gitlab.gwdg.de*). In both cases (GitHub and GitLab), certain features are only available for a fee (or for free but with restrictions).

**The Gitlab edition available at *gitlab.gwdg.de* is the Community Edition**, which offers numerous features, including CI/CD. In the GitLab documentation, the features contain notes about which edition they are available in, and starting with which version.

## Further reading

A very helpful **introduction to Git** including suggestions for further reading, created by **Jasmin Oster**, is available here: 
* https://pad.gwdg.de/vIbsAyj4S-C5Ko9ojhAZpw (in German)
* https://pad.gwdg.de/SC882NozQMWaOX_CraJLBg (in English - translated)

**Useful search terms:** "github vs. gitlab", "git vs. github", "VCS", "SCM".
