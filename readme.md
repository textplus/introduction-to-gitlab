# Overview

* [001_intro.md](001_intro.md): A few general facts about Gitlab, GitHub and Git
* [002_gitlab_official_docs.md](002_gitlab_official_docs.md): Some details about the Gitlab documentation
* [003_UI.md](003_UI.md): An overview of the features available via the Gitlab user interface

